import math

class Quaternion:
	def __init__(self, w=1,x=0, y=0, z=0):
		self.w = w
		self.x = x
		self.y = y
		self.z = z

	def __mul__(self, other):
		if isinstance(other, Quaternion):
			w1 = self.w
			x1 = self.x
			y1 = self.y
			z1 = self.z

			w2 = other.w
			x2 = other.x
			y2 = other.y
			z2 = other.z

			w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
			x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
			y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
			z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2

			return Quaternion(w, x, y, z)

		else:
			return Quaternion(self.w * other, self.x * other, self.y * other, self.z * other)

	def __add__(self, other):
		return Quaternion(self.w + other.w, self.x + other.x, self.y + other.y, self.z + other.z)

	def conjugate(self):
		return Quaternion(self.w , -self.x, -self.y, -self.z)

	def normalize(self):
		length = math.sqrt((self.w * self.w) + (self.x * self.x) + (self.y * self.y) + (self.z * self.z))
		return self * (1 / length)

	def matrix4(self):
		pass
