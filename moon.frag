#version 330 core

in float lambert;
in vec3 radiusVector;

out vec4 color;


void main() {

	if (length(radiusVector) < 4.5) {
		color = vec4(1, 1, 1, 1) * max(lambert, 0.1) * 0.5;
	} else {
		color = vec4(1, 1, 1, 1) * max(lambert, 0.1);
	}
}
