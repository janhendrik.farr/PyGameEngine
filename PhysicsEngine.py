import math
from vector3 import *
from quaternion import *
import numpy

class PhysicsEngine:
	def __init__(self, game):
		self.G = 6.67408 * 10**(-11)
		self.M = 7.342 * 10**(22)
		self.game = game
		self.timeWarp = 1.0

		self.a = Vector3()
		self.b = 0.0

		#print "asd"
		self.collisionGrid = Grid(self.game)
		#print "asd"

	def update(self):
		dt = self.timeWarp*self.game.getdt()

		direction =  self.game.player.position - self.game.moon.position
		#forces = direction.normalized * self.game.player.thrust
		forces = Vector3(0.0, self.game.player.thrust, 0.0)
		forces = forces.rotate(self.game.player.rotation)
		#forces += Vector3(0.0, -self.g * self.game.player.mass, 0.0)
		distance = direction.length
		forces += direction.normalized * ((-self.G * self.game.player.mass * self.M) * (1 / (distance * distance)))
		#print distance
		#print self.game.player.velocity.length, distance

		self.game.player.acceleration = forces * (1 / self.game.player.mass)
		self.game.player.velocity += self.game.player.acceleration * dt

		collision, collisions = self.game.player.cuboidCollider.moonCollision()
		if collision: #and self.game.player.thrust == 0.0:
			#self.game.player.velocity = Vector3(0.0, 0.0, 0.0)

			collisionDistances = []
			for collision in collisions:
				#vertex, normal, trianglePoint
				vertex = collision[0]
				normal = collision[1]
				trianglePoint = collision[2]

				absPosition = vertex.rotate(self.game.player.rotation) + self.game.player.position - self.game.moon.position - trianglePoint
				#print trianglePoint

				collisionDistances.append((normal.dot(absPosition)))

			index = getIndexOfMinVal(collisionDistances)
			self.game.player.position -= collisions[index][1] * collisionDistances[index]

			a = collisions[index][1].dot(self.game.player.velocity + self.game.player.angVelocity.cross(collisions[index][0].rotate(self.game.player.rotation)))
			previousVelocity = self.game.player.velocity

			if a < 0.0:
				self.game.player.velocity -= collisions[index][1] * a

			b = (self.game.player.velocity - previousVelocity).length * 0.8
			#b = 0.0
			if b > (self.game.player.velocity  + self.game.player.angVelocity.cross(collisions[index][0].rotate(self.game.player.rotation))).length:
			#if b > (self.game.player.velocity).length:
				self.game.player.velocity = Vector3(0.0, 0.0, 0.0)
			else:
				self.game.player.velocity -= (self.game.player.velocity + self.game.player.angVelocity.cross(collisions[index][0].rotate(self.game.player.rotation))).normalized * b
				#self.game.player.velocity -= (self.game.player.velocity).normalized * b

			appliedForce = (self.game.player.velocity - previousVelocity) * (1.0 / dt) * self.game.player.mass
			#print appliedForce
			collisionMoment = (collisions[index][0]).cross(appliedForce.rotate(self.game.player.rotation.conjugate()))

			self.a += collisionMoment
			self.b += 1.0
			print self.a * (1.0 / self.b)
		else:
			collisionMoment = Vector3(0.0, 0.0, 0.0)
		#collisionMoment = Vector3(0.0, 0.0, 0.0)
		#collisionMoment = Vector3(0.0, 0.0, 0.0)
		self.game.player.position += self.game.player.velocity * dt
		#collisionMoment = Vector3(0.0, 0.0, 0.0)
		if self.timeWarp > 1.0:
			self.game.player.moments = Vector3(0.0, 0.0, 0.0)
			collisionMoment = Vector3(0.0, 0.0, 0.0)
			self.game.player.angAcceleration = Vector3(0.0, 0.0, 0.0)
			self.game.player.angVelocity = Vector3(0.0, 0.0, 0.0)

		moments = self.game.player.moments + (collisionMoment * (1.0 / 10000.0))

		self.game.player.angAcceleration = moments * (1 / self.game.player.I)

		self.game.player.angVelocity += self.game.player.angAcceleration * dt
		angVelocity = Quaternion(0.0, self.game.player.angVelocity.x, self.game.player.angVelocity.y, self.game.player.angVelocity.z)

		self.game.player.rotation += self.game.player.rotation * (angVelocity * 0.5 * dt)
		self.game.player.rotation = self.game.player.rotation.normalize()

	def orbitCalc(self, position, velocity):
		absPosition = position - self.game.moon.position

		h = absPosition.cross(velocity)
		n = Vector3(0.0, 1.0, 0.0).cross(h)

		mu = self.G * self.M
		eVec = (absPosition * (velocity.length * velocity.length - (mu / absPosition.length)) - (velocity * (absPosition.dot(velocity)))) * (1 / mu)
		e = eVec.length

		E = ((velocity.length * velocity.length) / 2.0) - ((mu) / (absPosition.length))
		a = -(mu) / (2 * E)

		i = math.acos(h.y / h.length)
		#print n.x
		omega = math.acos(n.x / n.length)
		argPeri = math.acos((n.dot(eVec)) / (n.length * e))
		nu = math.acos((eVec.dot(absPosition)) / (e * absPosition.length))

		if n.z < 0.0:
			omega = 2 * math.pi - omega

		if eVec.y > 0.0:
			argPeri = 2 * math.pi - argPeri

		if absPosition.dot(velocity) < 0.0:
			nu = 2 * math.pi - nu

		return a, e, i, omega, argPeri, nu

def getIndexOfMinVal(lst):
	""" Find index of smallest value in a list """

	#initialize current min value and index to first element

	minIndex = 0 # index of current minimal value
	val = lst[0] # current minimal value

	# loop through all elements

	for i in range(1, len(lst)):

		# if current value is smaller than current minimum -> update values

		if lst[i] < val:
			minIndex = i
			val = lst[i]
	return minIndex

class CuboidCollider:
	def __init__(self, width, height, length, game, position=Vector3(), rotation=Quaternion()):
		self.verticies = [Vector3(0.0, -height / 2.0, -length / 2.0),
		                  Vector3(-width / 2.0, -height / 2.0, 0.0),
						  Vector3(0.0, -height / 2.0, length / 2.0),
						  Vector3(width / 2.0, -height / 2.0, 0.0),

						  Vector3(0.0, height / 2.0, -length / 2.0),
				  		  Vector3(-width / 2.0, height / 2.0, 0.0),
				  		  Vector3(0.0, height / 2.0, length / 2.0),
				  		  Vector3(width / 2.0, height / 2.0, 0.0),]

		self.position = position
		self.rotation = rotation

		self.game = game

	def moonCollision(self):
		gridKeys = []

		collision = False
		collisions = []

		self.position = self.game.player.position - self.game.moon.position

		for vertex in self.verticies:


			absPosition = vertex + self.position
			#xGrid = int(vertex.x / size) * size
			#if vertex.x < 0.0:
			#	xGrid -= size

			#yGrid = int(vertex.y / size) * size
			#if vertex.y < 0.0:
			#	yGrid -= size

			#zGrid = int(vertex.z / size) * size
			#if vertex.z < 0.0:
			#	zGrid -= size

			#key = (xGrid, yGrid, zGrid)

			key = self.game.physicsEngine.collisionGrid.getKeyfromPosition(absPosition)

			if not key in gridKeys:
				gridKeys.append(key)

		for key in gridKeys:

			if key in self.game.physicsEngine.collisionGrid.gridElements:
				gridElement = self.game.physicsEngine.collisionGrid.gridElements[key]

				collision = False
				#print len(gridElement.containedTriangles)
				for triangleIndex in gridElement.containedTriangles:

					for vertex in self.verticies:
						a, normal, trianglePoint = self.__triangleCollision(vertex.rotate(self.game.player.rotation), triangleIndex)
						if a:
							collision = True
							collisions.append([vertex, normal, trianglePoint])

		return collision, collisions

	def __triangleCollision(self, vertex, triangleIndex):
		normal = Vector3(self.game.moon.mesh.normals[triangleIndex], self.game.moon.mesh.normals[triangleIndex + 1], self.game.moon.mesh.normals[triangleIndex + 2])

		# TODO: make faster by exploiting the fact that these are sequential lookups
		# TODO: and store part of absPosition as variable to avoid recomputing
		trianglepoints = [Vector3(self.game.moon.mesh.verticies[triangleIndex], self.game.moon.mesh.verticies[triangleIndex + 1], self.game.moon.mesh.verticies[triangleIndex + 2]),
						  Vector3(self.game.moon.mesh.verticies[triangleIndex + 3], self.game.moon.mesh.verticies[triangleIndex + 4], self.game.moon.mesh.verticies[triangleIndex + 5]),
						  Vector3(self.game.moon.mesh.verticies[triangleIndex + 6], self.game.moon.mesh.verticies[triangleIndex + 7], self.game.moon.mesh.verticies[triangleIndex + 8])]

		absPosition = vertex + self.position - trianglepoints[0]

		if normal.dot(absPosition) > 0.0:
			return False, normal, trianglepoints[0]

		i = 0

		side = trianglepoints[(i + 1) % 3] - trianglepoints[i]

		sidenormal = normal.cross(side)

		if sidenormal.dot(trianglepoints[i] - trianglepoints[2]) > 0.0:
			direction = -1.0
			sidenormal *= -1.0
		else:
			direction = 1.0

		if sidenormal.dot(absPosition) < 0.0:
			return False, normal, trianglepoints[0]

		i += 1

		while i < 3:
			absPosition = vertex + self.position - trianglepoints[i]
			side = trianglepoints[(i + 1) % 3] - trianglepoints[i] * direction

			sidenormal = normal.cross(side)

			if sidenormal.dot(absPosition) < 0.0:
				return False, normal, trianglepoints[0]

			i += 1

		return True, normal, trianglepoints[0]

class Grid:
	def __init__(self, game):
		self.gridElements = {}
		size = 80000
		self.size = size

		for i in range(-2000000, 2000000, size):
			for j in range(-2000000, 2000000, size):
				#print j
				for k in range(-2000000, 2000000, size):
					self.gridElements[(i, j, k)] = GridElement()

		#print "starting"
		for i in range(0, len(game.moon.mesh.verticies), 9):
			contained = False
			previous = None
			for j in range(0, 9, 3):
				vertex = Vector3(game.moon.mesh.verticies[i + j], game.moon.mesh.verticies[i + j + 1], game.moon.mesh.verticies[i + j + 2])

				#xGrid = int(vertex.x / size) * size
				#if vertex.x < 0.0:
				#	xGrid -= size

				#yGrid = int(vertex.y / size) * size
				#if vertex.y < 0.0:
				#	yGrid -= size

				#zGrid = int(vertex.z / size) * size
				#if vertex.z < 0.0:
				#	zGrid -= size

				#element = (xGrid, yGrid, zGrid)
				element = self.getKeyfromPosition(vertex)

				if not previous == element:
					self.gridElements[element].addTriangle(i)
					previous = element

		for i in range(-2000000, 2000000, size):
			for j in range(-2000000, 2000000, size):
				#print j
				for k in range(-2000000, 2000000, size):
					self.gridElements[(i, j, k)].makeNumpy()

		#print "done"

	def getKeyfromPosition(self, position):
		xGrid = int(position.x / self.size) * self.size
		if position.x < 0.0:
			xGrid -= self.size

		yGrid = int(position.y / self.size) * self.size
		if position.y < 0.0:
			yGrid -= self.size

		zGrid = int(position.z / self.size) * self.size
		if position.z < 0.0:
			zGrid -= self.size

		return (xGrid, yGrid, zGrid)

class GridElement:
	def __init__(self):
		self.containedTriangles = []

		#for i in range(0, len(moon.mesh.verticies), 9):
		#	contained = False
		#	for j in range(0, 9, 3):
		#		vertex = Vector3(moon.mesh.verticies[i + j], moon.mesh.verticies[i + j + 1], moon.mesh.verticies[i + j + 2])
		#		#print vertex.x, vertex.y, vertex.z
		#		if vertex.x < x2 and vertex.x > x and vertex.y < y2 and vertex.y > y and vertex.z < z2 and vertex.z > z:
			#		contained = True
		#			#print "asd"

		#	if contained:
		#		self.containedTriangles.append(i)

		#self.containedTriangles = numpy.array(self.containedTriangles, dtype=numpy.int32)
	def addTriangle(self, index):
		self.containedTriangles.append(index)

	def makeNumpy(self):
		self.containedTriangles = numpy.array(self.containedTriangles, dtype=numpy.int32)
