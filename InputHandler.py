import pygame

class InputHandler:
	def __init__(self, game):
		self.game = game

	def update(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				self.game.quit()
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					self.game.quit()
				if event.key == pygame.K_LSHIFT:
					self.game.player.thrust = 51676.92
				if event.key == pygame.K_w:
					self.game.player.moments.x = -100.0
				if event.key == pygame.K_s:
					self.game.player.moments.x = 100.0
				if event.key == pygame.K_a:
					self.game.player.moments.z = 100.0
				if event.key == pygame.K_d:
					self.game.player.moments.z = -100.0
				if event.key == pygame.K_q:
					self.game.player.moments.y = 90.0
				if event.key == pygame.K_e:
					self.game.player.moments.y = -90.0
				if event.key == pygame.K_RIGHT:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.y = 1.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.y = 1.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.y = 1.0
				if event.key == pygame.K_LEFT:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.y = -1.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.y = -1.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.y = -1.0
				if event.key == pygame.K_UP:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.x = 1.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.x = 1.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.x = 1.0
				if event.key == pygame.K_DOWN:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.x = -1.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.x = -1.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.x = -1.0
				if event.key == pygame.K_PERIOD:
					if self.game.physicsEngine.timeWarp < 1000.0:
						self.game.physicsEngine.timeWarp *= 10.0
				if event.key == pygame.K_COMMA:
					if self.game.physicsEngine.timeWarp > 1.0:
						self.game.physicsEngine.timeWarp /= 10.0
				if event.key == pygame.K_m:
					if self.game.renderingEngine.scenes[0].active == True:
						 self.game.renderingEngine.scenes[0].active = False
						 self.game.renderingEngine.scenes[1].active = True
					else:
						 self.game.renderingEngine.scenes[0].active = True
						 self.game.renderingEngine.scenes[1].active = False

			elif event.type == pygame.KEYUP:
				if event.key == pygame.K_LSHIFT:
					self.game.player.thrust = 0.0
				if event.key == pygame.K_w:
					self.game.player.moments.x = 0.0
				if event.key == pygame.K_s:
					self.game.player.moments.x = 0.0
				if event.key == pygame.K_a:
					self.game.player.moments.z = 0.0
				if event.key == pygame.K_d:
					self.game.player.moments.z = 0.0
				if event.key == pygame.K_q:
					self.game.player.moments.y = 0.0
				if event.key == pygame.K_e:
					self.game.player.moments.y = 0.0
				if event.key == pygame.K_RIGHT:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.y = 0.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.y = 0.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.y = 0.0
				if event.key == pygame.K_LEFT:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.y = -0.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.y = -0.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.y = -0.0
				if event.key == pygame.K_UP:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.x = 0.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.x = 0.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.x = 0.0
				if event.key == pygame.K_DOWN:
					self.game.renderingEngine.scenes[0].cameraNear.angVelocity.x = -0.0
					self.game.renderingEngine.scenes[0].cameraFar.angVelocity.x = -0.0
					self.game.renderingEngine.scenes[1].camera.angVelocity.x = -0.0
