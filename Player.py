from GameObject import *
from vector3 import *
from objloader import *
from quaternion import *
from PhysicsEngine import *

class Player(DrawableGameObject):
	def __init__(self, game):
		DrawableGameObject.__init__(self, Obj("lander.obj"), 0, Vector3(0*1737110.0, 0*1737110.0+0000000, 2*1737110.0), Quaternion(), Vector3(1100.0, 10.0, 0.0), Vector3(0.0, 0.0, 0.0))

		self.mass = 15000.0
		self.I = 100.0

		self.moments = Vector3(0.0, 0.0, 0.0)

		self.thrust = 0.0

		self.cuboidCollider = CuboidCollider(9.4, 6.0, 9.4, game)
