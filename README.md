3D Moon Lander
==============

This game was written using OpenGL in Python 2.7. Unfortunatly I did not have the time
to finish it completely, however the main parts are all done. Mainly cosmetic features
like menus, dificulty level, etc is missing.

The files in the aivmath folder were taken from https://github.com/aiv01/aiv-python.
I have adapted some of them to my usage. All changes to those files are under GPL v2 License. All other files are licensed under GPL v2 as well, because GPL v2 requires so.

Controls
--------

* W / A / S / D - rotation
* Shift - turn on thrust
* m - map view
* . (dot) - increase time acceleration by 10x
* , (comma) - decrease time acceleration by 10x

Having time acceleration set to anything other then 1x stops all rotations.
This can be handy to stop the lander from rotating. Just hit . and then , to stop
any rotations.
It might seem like thrust is not working, however the acceleration is just very low
Just turn on time acceleration to go through burns faster.
