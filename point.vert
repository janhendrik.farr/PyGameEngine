#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
uniform mat4 local;
uniform mat4 camera;

void main() {
	gl_Position = camera * local * vec4(vertex, 1);
}
