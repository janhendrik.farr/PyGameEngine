import os
import numpy

class Mesh:
	def __init__(self, verticies, normals):
		self.verticies = numpy.array(verticies, dtype=numpy.float32)
		self.normals = numpy.array(normals, dtype=numpy.float32)

class Obj(Mesh):
	def __init__(self, filename):
		self.verticiesTmp = []
		self.vertexIndiciesTmp = []
		self.normalsTmp = []
		self.normalIndiciesTmp = []

		with open(filename, "r") as f:
			while True:
				line = f.readline()
				if line == '':
					f.close()
					break

				if line.startswith("v "):
					linesplit = line.split()
					self.verticiesTmp.append(float(linesplit[1]))
					self.verticiesTmp.append(float(linesplit[2]))
					self.verticiesTmp.append(float(linesplit[3]))
				elif line.startswith("vn "):
					linesplit = line.split()
					self.normalsTmp.append(float(linesplit[1]))
					self.normalsTmp.append(float(linesplit[2]))
					self.normalsTmp.append(float(linesplit[3]))
				elif line.startswith("f "):
					linesplit = line.split()

					for x in linesplit:
						if "//" in x:
							split = x.split("//")
							self.vertexIndiciesTmp.append(int(split[0]) - 1)
							self.normalIndiciesTmp.append(int(split[1]) - 1)
						elif "/" in x:
							split = x.split("/")
							self.vertexIndiciesTmp.append(int(split[0]) - 1)
							self.normalIndiciesTmp.append(int(split[2]) - 1)

		#self.verticies = numpy.array(self.verticies, dtype=numpy.float32)
		#self.vertexIndicies = numpy.array(self.vertexIndicies, dtype=numpy.int32)

		#self.normals = numpy.empty(len(self.vertexIndicies), dtype=numpy.float32)
		#for i in range(len(self.vertexIndicies)):
			#if not self.vertexIndicies[i] == i:
				#print i, self.vertexIndicies[i]
			#self.normals[i] = self.normalsTmp[self.vertexIndicies[i]]
		vertecies = []
		for i in range(len(self.vertexIndiciesTmp) * 3):
			vertecies.append(0.0)

		for i in range(len(self.vertexIndiciesTmp)):
			vertecies[3 * i] = self.verticiesTmp[self.vertexIndiciesTmp[i] * 3]
			vertecies[3 * i + 1] = self.verticiesTmp[self.vertexIndiciesTmp[i] * 3 + 1]
			vertecies[3 * i + 2] = self.verticiesTmp[self.vertexIndiciesTmp[i] * 3 + 2]

		#self.verticies = numpy.array(vertecies, dtype=numpy.float32)

		normals = []
		for i in range(len(self.normalIndiciesTmp) * 3):
			normals.append(0.0)

		for i in range(len(self.normalIndiciesTmp)):
			normals[i * 3] = self.normalsTmp[self.normalIndiciesTmp[i] * 3]
			normals[i * 3 + 1] = self.normalsTmp[self.normalIndiciesTmp[i] * 3 + 1]
			normals[i * 3 + 2] = self.normalsTmp[self.normalIndiciesTmp[i] * 3 + 2]

		#self.normals = numpy.array(normals, dtype=numpy.float32)

		Mesh.__init__(self, vertecies, normals)
