#version 330 core

in float lambert;
out vec4 color;

void main() {
	color = vec4(1, 1, 1, 1) * max(lambert, 0.1);
}
