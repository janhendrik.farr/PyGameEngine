from vector3 import *
import numpy
from objloader import *

class GameObject:
	def __init__(self, position=Vector3(), rotation=Quaternion(), velocity=Vector3(), angVelocity=Vector3(), components=[]):

		self.position = position
		self.rotation = rotation

		self.velocity = velocity
		self.angVelocity = angVelocity

		self.acceleration = Vector3(0.0, 0.0, 0.0)
		self.angAcceleration = Vector3(0.0, 0.0, 0.0)#Quaternion()

		self.components = components

	def addComponent(self, component):
		self.components.append(component)

	def preUpdate(self):
		for component in self.components:
			component.preUpdate

	def preRender(self):
		for component in self.components:
			component.preRender

class DrawableGameObject(GameObject):
	def __init__(self, mesh, shaderIndex, position=Vector3(), rotation=Quaternion(), velocity=Vector3(), angVelocity=Vector3(), components=[]):
		GameObject.__init__(self, position, rotation, velocity, angVelocity, components)

		self.mesh = mesh
		self.shaderIndex = shaderIndex
		self.vertexArrayPosition = None

class Point(GameObject):
	def __init__(self, mesh, shaderIndex, position, velocity=Vector3(), angVelocity=Vector3(), components=[]):
		GameObject.__init__(self, position, Quaternion(), velocity, angVelocity, components)

class Point(DrawableGameObject):
	def __init__(self):
		vertecies = [0.0, 0.0, 0.0]
		normals = [0.0, 0.0, 0.0]
		DrawableGameObject.__init__(self, Mesh(vertecies, normals), 2)

class Circle(DrawableGameObject):
	def __init__(self, radius):
		self.radius = radius
		self.vertexArrayPosition = None

		verticies, normals = self.__genMesh(100)
		DrawableGameObject.__init__(self, Mesh(verticies, normals), 1)

	def __genMesh(self, n):
		vertecies = []
		normals = []

		angle = (float(0)) / (float(n)) * 2 * math.pi
		vertecies.append(math.cos(angle) * self.radius)
		vertecies.append(0.0)
		vertecies.append(math.sin(angle) * self.radius)

		for i in range(1, n + 1):
			angle = (float(i)) / (float(n)) * 2 * math.pi
			vertecies.append(math.cos(angle) * self.radius)
			vertecies.append(0.0)
			vertecies.append(math.sin(angle) * self.radius)
			vertecies.append(math.cos(angle) * self.radius)
			vertecies.append(0.0)
			vertecies.append(math.sin(angle) * self.radius)

			normals.append(0.0)
			normals.append(0.0)
			normals.append(0.0)



		return vertecies, normals

class GameObjectComponent:
	def __init__(self, gameObject):
		self.gameObject = gameObject

	def preUpdate(self):
		pass

	def preRender(self):
		pass
