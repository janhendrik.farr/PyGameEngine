import os

import pygame
from OpenGL.GL import *

import numpy
from aivmath.matrix4 import Matrix4
from vector3 import *
from objloader import *

class RenderingEngine:
	def __init__(self, game, gameObjects, width, height):
		self.game = game
		self.gameObjects = gameObjects

		self.width = width
		self.height = height

		self.__initializePyGame(width, height, False)
		self.__initializeOpenGL()

		self.activeShaderProgram = None

		self.__uploadData()

		self.floatingOrigin = FloatingOrigin(self.game, self)

		self.scenes = []

	def addScene(self, scene):
		self.scenes.append(scene)

	def __initializePyGame(self, width, height, fullScreen):
		pygame.init()
		if os.name == "nt":
			ctypes.windll.user32.SetProcessDPIAware()
		if not fullScreen:
			self.screen = pygame.display.set_mode((width, height),pygame.DOUBLEBUF|pygame.OPENGL)

	def __initializeOpenGL(self):
		print glGetString(GL_VERSION)
		glClearColor(0.0, 0.0, 0.0, 1.0)

		glEnable(GL_DEPTH_TEST)

		glLineWidth(3.0)
		glEnable(GL_LINE_SMOOTH)

		glPointSize(15.0)
		glEnable(GL_POINT_SMOOTH)

		self.__loadShaders()

		self.vao = glGenVertexArrays(1)
		glBindVertexArray(self.vao)

	def __loadShaders(self):
		self.programs = []

		vertexShader1 = Shader("shader.vert", GL_VERTEX_SHADER)
		fragmentShader1 = Shader("shader.frag", GL_FRAGMENT_SHADER)

		program1 = ShaderProgram(vertexShader1, fragmentShader1)

		self.programs.append(program1)

		vertexShader2 = Shader("orbit.vert", GL_VERTEX_SHADER)
		fragmentShader2 = Shader("orbit.frag", GL_FRAGMENT_SHADER)

		program2 = ShaderProgram(vertexShader2, fragmentShader2)

		self.programs.append(program2)

		vertexShader3 = Shader("point.vert", GL_VERTEX_SHADER)
		fragmentShader3 = Shader("point.frag", GL_FRAGMENT_SHADER)

		program3 = ShaderProgram(vertexShader3, fragmentShader3)

		self.programs.append(program3)

		vertexShader4 = Shader("moon.vert", GL_VERTEX_SHADER)
		fragmentShader4 = Shader("moon.frag", GL_FRAGMENT_SHADER)

		program4 = ShaderProgram(vertexShader4, fragmentShader4)

		self.programs.append(program4)

	def __uploadData(self):
		self.vao = glGenVertexArrays(1)
		glBindVertexArray(self.vao)

		self.__uploadVertecies()
		self.__uploadNormals()

	def __uploadVertecies(self):
		self.vbo = glGenBuffers(1)
		glBindBuffer(GL_ARRAY_BUFFER, self.vbo)

		numberOfBytes = 0
		currentDrawCount = 0

		for gameObject in self.gameObjects:
			gameObject.vertexArrayPosition = currentDrawCount
			numberOfBytes += gameObject.mesh.verticies.nbytes
			currentDrawCount += len(gameObject.mesh.verticies) / 3

		glBufferData(GL_ARRAY_BUFFER, numberOfBytes, None, GL_STATIC_DRAW)

		nextIndex = 0

		for gameObject in self.gameObjects:
			glBufferSubData(GL_ARRAY_BUFFER, nextIndex, gameObject.mesh.verticies.nbytes, gameObject.mesh.verticies)
			nextIndex += gameObject.mesh.verticies.nbytes

		glEnableVertexAttribArray(0)
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)

	def __uploadNormals(self):
		self.vbo_normals = glGenBuffers(1)
		glBindBuffer(GL_ARRAY_BUFFER, self.vbo_normals)

		numberOfBytes = 0

		for gameObject in self.gameObjects:
			numberOfBytes += gameObject.mesh.normals.nbytes

		glBufferData(GL_ARRAY_BUFFER, numberOfBytes, None, GL_STATIC_DRAW)

		nextIndex = 0

		for gameObject in self.gameObjects:
			glBufferSubData(GL_ARRAY_BUFFER, nextIndex, gameObject.mesh.normals.nbytes, gameObject.mesh.normals)
			nextIndex += gameObject.mesh.normals.nbytes

		glEnableVertexAttribArray(1)
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, None)

	def clearScreen(self):
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

	def setActiveShaderProgram(self, program):
		if not self.activeShaderProgram == program:
			self.activeShaderProgram = program
			self.activeShaderProgram.enable()

	def draw(self):
		for scene in self.scenes:
			if scene.active:
				scene.render()

	def flipBuffers(self):
		pygame.display.flip()

class ShaderProgram:
	def __init__(self, vertexShader, fragmentShader):
		self.programID = glCreateProgram()

		self.vertexShader = vertexShader
		self.fragmentShader = fragmentShader

		self.__link()

		self.__checkError()

		self.local = glGetUniformLocation(self.programID, "local")
		self.camera = glGetUniformLocation(self.programID, "camera")

	def __link(self):
		glAttachShader(self.programID, self.vertexShader.shaderID)
		glAttachShader(self.programID, self.fragmentShader.shaderID)

		glLinkProgram(self.programID)

	def __checkError(self):
		result = glGetProgramiv(self.programID, GL_LINK_STATUS)

		if (result!=1):
			raise Exception("Couldn't link shader\nERROR:\n" + glGetProgramInfoLog(self.programID))

	def enable(self):
		glUseProgram(self.programID)

class Shader:
	def __init__(self, filename, shaderType):
		shaderSource = self.__readFile(filename)

		self.shaderID = glCreateShader(shaderType)

		self.__compile(shaderSource)

		self.__checkError()

	def __readFile(self, filename):
		f = open(filename, "r")
		lines = f.readlines()
		f.close()

		return lines

	def __compile(self, source):
		glShaderSource(self.shaderID, source)
		glCompileShader(self.shaderID)

	def __checkError(self):
		result = glGetShaderiv(self.shaderID, GL_COMPILE_STATUS)

		if (result!=1):
			raise Exception("Couldn't compile shader\nERROR:\n" + glGetShaderInfoLog(self.shaderID))

class FloatingOrigin:
	def __init__(self, game, renderingEngine):
		self.game = game
		self.renderingEngine = renderingEngine

		self.position32 = numpy.array([0.0, 0.0, 0.0], dtype=numpy.float32)

		self.update()

	@property
	def position(self):
		return Vector3(self.position32[0], self.position32[1], self.position32[2])

	def update(self):
		delta = self.game.player.position
		if delta.length >= 10000.0:
			previousPosition = self.position
			self.position32[0] += self.game.player.position.x
			self.position32[1] += self.game.player.position.y
			self.position32[2] += self.game.player.position.z


			for gameObject in self.game.gameObjects:
				gameObject.position -= (self.position - previousPosition)

class Scene:
	def __init__(self, game, gameObjects):
		self.game = game
		self.gameObjects = gameObjects
		self.active = False

	def render(self):
		raise NotImplementedError

class WorldScene(Scene):
	def __init__(self, game, gameObjects):
		Scene.__init__(self, game, gameObjects)

		self.cameraFar = Camera3rdPerson(self.game, game.renderingEngine, self, 60.0, 500.0, 4000000.0, 40.0)
		self.cameraNear = Camera3rdPerson(self.game, game.renderingEngine, self, 60.0, 1.0, 500.0, 40.0)

	def render(self):
		self.cameraFar.draw()
		glClear(GL_DEPTH_BUFFER_BIT)
		self.cameraNear.draw()

class MapScene(Scene):
	def __init__(self, game, gameObjects):
		Scene.__init__(self, game, gameObjects)

		self.camera = MapCamera(self.game, game.renderingEngine, self, 50.0, 0.1, 50.0, 10.0)

	def render(self):
		self.camera.draw()

class Camera3rdPerson:
	def __init__(self, game, renderingEngine, scene, fov, near, far, zoom=40):
		self.game = game
		self.renderingEngine = renderingEngine
		self.scene = scene

		self.zoom = zoom
		self.fov = fov
		self.near = near
		self.far = far

		self.angles = [0.0, 0.0]
		self.angVelocity = Vector3(0.0, 0.0, 0.0)
		self.direction = Quaternion()
		self.position = Vector3(0.0, 0.0, self.zoom)

		self.playerPositionID = glGetUniformLocation(self.renderingEngine.programs[3].programID, "playerPosition")

	def __preRender(self):
		dt = self.game.getdt()
		self.angles[1] += self.angVelocity.y * dt
		self.angles[0] += self.angVelocity.x * dt

		self.position = self.game.player.position + Vector3(math.cos(self.angles[0]) * math.sin(self.angles[1]) * self.zoom, math.sin(self.angles[0]) * self.zoom, math.cos(self.angles[0]) * math.cos(self.angles[1]) * self.zoom)

	def draw(self):
		self.__preRender()

		self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[0])

		cam = Matrix4.perspective(self.fov, float(self.renderingEngine.width) / float(self.renderingEngine.height), self.near, self.far) * Matrix4.look_at(self.position.x, self.position.y, self.position.z, self.game.player.position.x, self.game.player.position.y, self.game.player.position.z, 0.0, 1.0, 0.0)
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)

		for gameObject in self.scene.gameObjects:
			if gameObject == self.game.moon:
				self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[3])
				cam = Matrix4.perspective(self.fov, float(self.renderingEngine.width) / float(self.renderingEngine.height), self.near, self.far) * Matrix4.look_at(self.position.x, self.position.y, self.position.z, self.game.player.position.x, self.game.player.position.y, self.game.player.position.z, 0.0, 1.0, 0.0)
				glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)
				playerPosition = self.game.player.position - (self.game.player.position - self.game.moon.position).normalized * 2.5
				#glUniform3f(self.playerPositionID, 1, GL_FALSE, numpy.array([playerPosition.x, playerPosition.y, playerPosition.z], dtype=numpy.float32))
				glUniform3f(self.playerPositionID, playerPosition.x, playerPosition.y, playerPosition.z)
			else:
				self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[0])
				cam = Matrix4.perspective(self.fov, float(self.renderingEngine.width) / float(self.renderingEngine.height), self.near, self.far) * Matrix4.look_at(self.position.x, self.position.y, self.position.z, self.game.player.position.x, self.game.player.position.y, self.game.player.position.z, 0.0, 1.0, 0.0)
				glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)

			position = gameObject.position
			rotation = gameObject.rotation

			local = Matrix4.translate(position.x, position.y, position.z) * Matrix4.fromQuaternion(rotation)
			glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.local, 1, GL_FALSE, local.m)

			glDrawArrays(GL_TRIANGLES, gameObject.vertexArrayPosition, len(gameObject.mesh.verticies) / 3)



class MapCamera:
	def __init__(self, game, renderingEngine, scene, fov, near, far, zoom=10.0):
		self.game = game
		self.renderingEngine = renderingEngine
		self.scene = scene

		self.scale = 1.0 / 1737100.0

		self.zoom = zoom
		self.fov = fov
		self.near = near
		self.far = far

		self.angles = [0.0, 0.0]
		self.angVelocity = Vector3(0.0, 0.0, 0.0)
		self.direction = Quaternion()
		self.position = Vector3(0.0, 0.0, self.zoom)

	def __preRender(self):
		dt = self.game.getdt()

		dt = self.game.getdt()
		self.angles[1] += self.angVelocity.y * dt
		self.angles[0] += self.angVelocity.x * dt

		self.position = Vector3(math.cos(self.angles[0]) * math.sin(self.angles[1]) * self.zoom, math.sin(self.angles[0]) * self.zoom, math.cos(self.angles[0]) * math.cos(self.angles[1]) * self.zoom)

	def draw(self):
		self.__preRender()

		self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[0])

		cam = Matrix4.perspective(self.fov, float(self.renderingEngine.width) / float(self.renderingEngine.height), self.near, self.far) * Matrix4.look_at(self.position.x, self.position.y, self.position.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0) * Matrix4.scale(self.scale, self.scale, self.scale)
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)

		for gameObject in self.scene.gameObjects[:-2]:
			position = gameObject.position - self.game.moon.position
			rotation = gameObject.rotation

			local = Matrix4.translate(position.x, position.y, position.z) * Matrix4.fromQuaternion(rotation)
			glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.local, 1, GL_FALSE, local.m)

			glDrawArrays(GL_TRIANGLES, gameObject.vertexArrayPosition, len(gameObject.mesh.verticies) / 3)

		self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[1])

		a, e, i, omega, argPeri, nu = self.game.physicsEngine.orbitCalc(self.game.player.position, self.game.player.velocity)

		#print a, e, i, omega, argPeri, nu
		if not e >= 1.0:
			local = Matrix4.rotate_y(-omega * 180.0 / math.pi) * Matrix4.rotate_x(i * 180.0 / math.pi) * Matrix4.rotate_y(-argPeri * 180.0 / math.pi) * Matrix4.translate(-a * e, 0.0, 0.0) * Matrix4.scale(a, 0.0, a * math.sqrt(1 - (e * e)))
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.local, 1, GL_FALSE, local.m)
		glDrawArrays(GL_LINES, self.scene.gameObjects[-2].vertexArrayPosition, len(self.scene.gameObjects[-2].mesh.verticies) / 3)

		self.renderingEngine.setActiveShaderProgram(self.renderingEngine.programs[2])

		playerPosition = self.game.player.position - self.game.moon.position
		local = Matrix4.translate(playerPosition.x, playerPosition.y, playerPosition.z)
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.camera, 1, GL_FALSE, cam.m)
		glUniformMatrix4fv(self.renderingEngine.activeShaderProgram.local, 1, GL_FALSE, local.m)
		glDrawArrays(GL_POINTS, self.scene.gameObjects[-1].vertexArrayPosition, len(self.scene.gameObjects[-1].mesh.verticies) / 3)
