import pygame
from InputHandler import *
from RenderingEngine import *
from PhysicsEngine import *
from Player import *
from objloader import *

class Game:
	def __init__(self):
		#self.frameCount = 0
		#self.time = 0.0
		self.isRunning = False

		self.player = Player(self)
		self.moon = DrawableGameObject(Obj("moon.obj"), 0, Vector3(0.0, 0.0, 0.0), Quaternion(), Vector3(0.0, 0.0, 0.0), Vector3(0.0, 0.0, 0.0))
		#print len(self.moon.mesh.verticies)
		self.orbit = Circle(1.0)
		self.point = Point()
		#testGridElement = GridElement(0.0, 0.0, 0.0, 2000000, self.moon)
		#print testGridElement.containedTriangles

		self.gameObjects = []
		self.gameObjects.append(self.player)
		self.gameObjects.append(self.moon)
		self.gameObjects.append(self.orbit)

		drawableObjects = []
		drawableObjects.append(self.player)
		drawableObjects.append(self.moon)
		drawableObjects.append(self.orbit)
		drawableObjects.append(self.point)

		self.inputHandler = InputHandler(self)

		self.renderingEngine = RenderingEngine(self, drawableObjects, 1500, 1000)
		worldScene = WorldScene(self, drawableObjects)
		worldScene.active = True
		self.renderingEngine.addScene(worldScene)

		mapScene = MapScene(self, [self.moon, self.orbit, self.point])
		self.renderingEngine.addScene(mapScene)

		self.physicsEngine = PhysicsEngine(self)

		self.clock = pygame.time.Clock()

	def run(self):
		self.isRunning = True

		while self.isRunning:
			self.__input()
			self.__update()
			self.__render()

	def __input(self):
		self.inputHandler.update()

	def __update(self):
		self.__dt = min(float(self.clock.tick(60)) / 1000.0, 0.1)
		#self.time += self.__dt
		#self.frameCount += 1
		#if not self.time == 0.0:
		#	print self.frameCount / self.time
		#if self.time > 2.0:
		#	self.time = 0.0
		#	self.frameCount = 0
		#print dt
		self.renderingEngine.floatingOrigin.update()

		for gameObject in self.gameObjects:
			gameObject.preUpdate()

		self.physicsEngine.update()
		#print self.physicsEngine.orbitCalc(self.player.position, self.player.velocity)

	def __render(self):
		for gameObject in self.gameObjects:
			gameObject.preRender()

		self.renderingEngine.clearScreen()
		self.renderingEngine.draw()
		self.renderingEngine.flipBuffers()

	def quit(self):
		self.isRunning = False

	def getdt(self):
		return self.__dt

if __name__ == "__main__":
	game = Game()
	game.run()
