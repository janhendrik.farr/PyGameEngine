class Vector3D:
	def __init__(self, x, y, z):
		""" initialize x and y component """

		self.x = x
		self.y = y
		self.z = z

	def __add__(self, other):
		""" adds two vectors and returns result """

		return Vector3D(self.x + other.x, self.y + self.y, self.z + other.z)

	def __sub__(self, other):
		""" subtracts two vectors and returns result """

		return Vector3D(self.x - other.x, self.y - self.y, self.z - other.z)

	def __mul__(self, other):
		""" multiplies vector with scalar and returns result """

		return Vector3D(self.x * other, self.y * other, self.z * other)
