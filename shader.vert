#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
uniform mat4 local;
uniform mat4 camera;

out float lambert;

void main() {
	vec3 light_direction = vec3(0.4082482905, -0.8164965809, 0.4082482905);

	gl_Position = camera * local * vec4(vertex, 1);
	//gl_Position.z = (2*log(1.0f * gl_Position.w + 1) / log(1.0f * 4000000.0 + 1) - 1) * gl_Position.w;

	//const float C = 0.0000001;//0.000001;
  //const float far = 4000000.0;
  //const float offset = 1.0;
  //gl_Position.z = (2*log(C * gl_Position.z + offset) / log(C * far + offset)-1);

	vec4 normal_world = local * vec4(normal, 0);

	lambert = max(dot(light_direction, normal_world.xyz), 0);
}
