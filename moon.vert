#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
uniform mat4 local;
uniform mat4 camera;
uniform vec3 playerPosition;

out float lambert;
out vec3 radiusVector;

void main() {
	//vec3 playerPosition = vec3(0.0f, 0.0f, 1737110.0f);
	vec3 light_direction = vec3(0.4082482905, -0.8164965809, 0.4082482905);

	vec4 vertex_world = local * vec4(vertex, 1);
	gl_Position = camera * local * vec4(vertex, 1);
	vec4 normal_world = local * vec4(normal, 0);

	lambert = max(dot(light_direction, normal_world.xyz), 0);

	float d = (dot((vertex_world.xyz - playerPosition), normal_world.xyz)) / (dot(light_direction, normal_world.xyz));
	radiusVector = (d * light_direction) + playerPosition - vertex_world.xyz;
}
